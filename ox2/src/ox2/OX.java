package ox2;

import java.util.*;

public class OX {

    private static Scanner kb = new Scanner(System.in);
    
   
    
     
      
       
    private static String board[][] = {{" ", " ", " "},
    {" ", " ", " "},
    {" ", " ", " "}};

    private static String turn = "O";
    
    private static String test;

    public static void main(String[] args) {
        printStart();

        while (true) {
            printBoard();
            inputPosition();
            if (checkWin()) {
                printWin();
                break;
            } else if (checkDraw()) {
                printDraw();
                break;
            }
            switchTurn();
        }
    }

    private static void printStart() {
        System.out.println("Start Game OX");
    }

    private static void printBoard() {

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {
                System.out.print("|" + board[i][j]);
            }

            System.out.println("|");
            if (i == 0 || i == 1) {
                System.out.println("--------");
            }

        }
    }

    private static boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == turn && board[i][1] == turn && board[i][2] == turn) {
                return true;
            }
            if (board[0][i] == turn && board[1][i] == turn && board[2][i] == turn) {
                return true;
            }
        }
        if (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) {
            return true;
        }
        if (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
            return true;
        }
        return false;
    }
    
    

    private static void inputPosition() {
       
        while (true) {
                printTurn();

            try {
                int R = Integer.parseInt(kb.next()) - 1;
                int C = Integer.parseInt(kb.next()) - 1;
                int[] number = new int[3];
                number[0] = R;
                number[1] = C;
                if(number[2] == 0){
                    System.out.print("U SHOULD INPUT 2 THING 1. ROW 2. COLUMN");
                    break;
                    }
                
                if (R > 2 || R < 0 || C < 0 || C > 2) {
                    System.out.println("Row and Column must be number 1 - 3");
                    printBoard();
                    continue;
                } else if (!board[R][C].equals(" ")) {
                    System.out.println("Row " + (R + 1) + " and Column " + (C + 1) + " can't choose again");
                    printBoard();
                    continue;
                }
                board[R][C] = turn;
                break;
            } catch (Exception a) {
                System.out.println("Row and Column must be number");
                printBoard();
                continue;
            }

        }
    }

    private static boolean checkDraw() {
        boolean chk = false;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == " ") {
                    chk = true;
                }
            }
        }
        if (chk == false) {
            return true;
        }
        return false;
    }

    private static void switchTurn() {
        if (turn.equals("X")) {
            turn = "O";
        } else {
            turn = "X";
        }
    }

    private static void printWin() {
        printBoard();
        System.out.print(turn + " : WIN");
    }

    private static void printDraw() {
        printBoard();
        System.out.print("DRAW");
    }
    
      private static void printTurn() {
        System.out.println("Turn: " + turn);
        System.out.print("Choose position (R,C) :");
    
    }
}

